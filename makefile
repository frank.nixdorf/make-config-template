include ./MyVars.mak

dummy:
	@echo My private variable is $(myvar) > dummy

clean:
	rm dummy

clean-all:
	rm dummy; echo myvar:= TODO > MyVars.mak
