# make-config-template

A simple example for setting up a makefile in a project with version control and usage of external paths that depend on the machine. 

#Usage

Create a file (e.g. "MyVars.mak") and include the line "include ./MyVars.mak" in your makefile. Assign in the makefile all dependent variables with:

	myvariable1 := myvalue
	myvariable2 := somethingelse
	.
	.
	.

You can use in the makefile with the usual "$(myvariable1), $(myvariable2), ...". Next you want to either add "MyVars.mak" in your .gitignore or alternatively call "git update-index --skip-worktree MyVars.mak" in your terminal. 
The first option tells git to completly ignore MyVars.mak, after a push other users will also have their MyVars.mak ignored. You can undo that by removing the line from -gitignore.
The second option tells git to avoid checking if MyVars.mak has been modified. The only takes place locally, meaning everyone should use it. If you pull a commit with an updated MyVars.mak git will tell you (unless you configured it differently) to merge your MyVars.mak. This can be useful if you want to be notified by git if MyVars.mak has to be changed. You can undo that by calling "git update-index --no-skip-worktree MyVars.mak".
